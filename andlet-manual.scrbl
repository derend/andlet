#lang scribble/manual
@(require scribble/eval
          racket/sandbox
          "main.rkt"
          (for-label racket
                     "main.rkt"
                     )
          )
@(define (author-email) "deren.dohoda@gmail.com")

@title[#:tag "top"]{Andlet}
@author{@(author+email "Deren Dohoda" (author-email))}

@(define eval (parameterize ((sandbox-output 'string)
                             (sandbox-error-output 'string)
                             (sandbox-memory-limit 10))
                (make-evaluator 'racket/base #:requires '("main.rkt"))))

@defmodule[andlet]

@section{Andlet}
The provided forms allow one to combine the basic Racket @racket[and] logic with @racket[let].

@defform[(andlet ([id val-expr #:test maybe-proc] ...) expr ...)]{Like Racket's @racket[let],
 except the result of each expression @code{val-exp} in the @code{(id val-exp)} pairs is checked to be not @code{#f}. Any time a @code{val-expr}
 evaluates to @code{#f}, the entire @racket[andlet] expression is @code{#f} and subsequent bindings are not evaluated.

 If a different condition than merely checking for falisty is required,
 use the optional argument @code{#:test} to give a procedure to use for generating the @code{#t} or @code{#f} value. Like Racket, anything which isn't
 @code{#f} is considered @code{#t}.

 Unlike Racket's @racket[let], @racket[andlet] is not capable of the named-let pattern of creating a procedure.}

@examples[#:eval eval
          (andlet ((a 10)
                   (b 20 #:test number?)
                   (c 'q #:test number?)
                   (d (/ 1 0)))
            (displayln c) (+ a b))]

@defform[(andlet* ([id val-exp #:test maybe-proc] ...) expr ...)]{Like @racket[andlet], except as with Racket's @racket[let*], bindings are sequentially
 introduced so subsequent @code{val-exp}s may refer to the bound @code{id}s of
 previous name/value pairs.}

@examples[#:eval eval
          (andlet* ((a 10)
                    (b (+ a 10))
                    (a 20))
            (+ a b))]